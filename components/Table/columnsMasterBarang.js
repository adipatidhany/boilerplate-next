import utils from "../../Services/utils";

export const columns = [
  { title: "ID Barang", field: "idBarang" },
  { title: "Nama Barang", field: "name", cellStyle: { whiteSpace: "nowrap" } },
  { title: "ID Supplier", field: "supplierId" },
  { title: "Vendor", field: "vendor", cellStyle: { whiteSpace: "nowrap" } },
  { title: "Dimensi", field: "dimension", cellStyle: { whiteSpace: "nowrap" } },
  { title: "Stok", field: "quantity", type: "numeric" },
  {
    title: "Tanggal Registrasi",
    field: "createdAt",
    cellStyle: { whiteSpace: "nowrap" },
    render: (rowData) => {
      if (rowData.createdAt === null) {
        return 0;
      }
      return utils.formatDate(rowData.createdAt);
    },
  },
  {
    title: "Tanggal Diperbaharui",
    field: "updatedAt",
    cellStyle: { whiteSpace: "nowrap" },
    render: (rowData) => {
      if (rowData.createdAt === null) {
        return 0;
      }
      return utils.formatDate(rowData.createdAt);
    },
  },
];
