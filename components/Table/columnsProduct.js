export const columnsProduct = [
  { title: "ID Barang", field: "barangId" },
  { title: "Nama Barang", field: "name", cellStyle: { whiteSpace: "nowrap" } },
  { title: "Stok", field: "quantity", type: "numeric" },
];
