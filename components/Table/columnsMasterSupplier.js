import utils from "../../Services/utils";

export const columns = [
  { title: "ID Supplier", field: "supplierId" },
  { title: "Nama Supplier", field: "name", cellStyle: { whiteSpace: "nowrap" } },
  { title: "Vendor", field: "vendor", cellStyle: { whiteSpace: "nowrap" } },
  {
    title: "Tanggal Registrasi",
    field: "createdAt",
    cellStyle: { whiteSpace: "nowrap" },
    render: (rowData) => {
      if (rowData.createdAt === null) {
        return 0;
      }
      return utils.formatDate(rowData.createdAt);
    },
  },
  {
    title: "Tanggal Diperbaharui",
    field: "updatedAt",
    cellStyle: { whiteSpace: "nowrap" },
    render: (rowData) => {
      if (rowData.createdAt === null) {
        return 0;
      }
      return utils.formatDate(rowData.createdAt);
    },
  },
];
