import Link from 'next/link';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import ListAltIcon from '@material-ui/icons/ListAlt';

export const mainListItems = (
  <div>
    <Link href="/admin/dashboard">
      <Tooltip title="Dashboard" placement="right">
        <ListItem button>
          <ListItemIcon>
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItem>
      </Tooltip>
    </Link>
    <Link href="/admin/supplier">
      <Tooltip title="Master Supplier" placement="right">
        <ListItem button>
          <ListItemIcon>
            <PeopleIcon />
          </ListItemIcon>
          <ListItemText primary="Master Supplier" />
        </ListItem>
      </Tooltip>
    </Link>
    <Link href="/admin/barang">
      <Tooltip title="Master Barang" placement="right">
        <ListItem button>
          <ListItemIcon>
            <AccountBalanceIcon />
          </ListItemIcon>
          <ListItemText primary="Master Barang" />
        </ListItem>
      </Tooltip>
    </Link>
    <Link href="/admin/invoice">
      <Tooltip title="Invoices" placement="right">
        <ListItem button>
          <ListItemIcon>
            <ListAltIcon />
          </ListItemIcon>
          <ListItemText primary="Invoices" />
        </ListItem>
      </Tooltip>
    </Link>
    <Link href="/admin/product">
      <Tooltip title="Product" placement="right">
        <ListItem button>
          <ListItemIcon>
            <ShoppingCartIcon />
          </ListItemIcon>
          <ListItemText primary="Product" />
        </ListItem>
      </Tooltip>
    </Link>
  </div>
);
