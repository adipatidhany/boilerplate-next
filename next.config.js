require('dotenv').config()

module.exports = {
  env: {
    key: process.env.key,
    urlBarang: process.env.urlBarang,
    urlSupplier: process.env.urlSupplier,
    urlInvoice: process.env.urlInvoice,
  },
}