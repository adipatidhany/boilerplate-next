import axios from "axios";

const getInvoice = async () => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  await axios
    .get(process.env.urlInvoice, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
    });
  return data;
};

const updateInvoice = async (invoiceId, keterangan) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const url = `${process.env.urlInvoice}/${invoiceId}`;
  const requestBody = {
    keterangan
  };
  await axios
    .patch(url, requestBody, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const deleteInvoice = async (invoiceId) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const url = `${process.env.urlInvoice}/${invoiceId}`;
  await axios
    .delete(url, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const addInvoice = async (invoiceId, name) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const requestBody = {
    invoiceId,
    name
  };
  await axios
    .post(process.env.urlInvoice, requestBody, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const invoiceServices = { getInvoice, updateInvoice, deleteInvoice, addInvoice };

export default invoiceServices;
