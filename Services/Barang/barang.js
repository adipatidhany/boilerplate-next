import axios from "axios";

const getBarang = async () => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  await axios
    .get(process.env.urlBarang, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
    });
  return data;
};

const updateBarang = async (idBarang, name, quantity) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const url = `${process.env.urlBarang}/${idBarang}`;
  const requestBody = {
    name,
    quantity
  };
  await axios
    .patch(url, requestBody, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const deleteBarang = async (idBarang) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const url = `${process.env.urlBarang}/${idBarang}`;
  await axios
    .delete(url, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const addBarang = async (supplierId, idBarang, name, quantity, dimension) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const requestBody = {
    supplierId,
    idBarang,
    name,
    quantity,
    dimension,
  };
  await axios
    .post(process.env.urlBarang, requestBody, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const barangServices = { getBarang, updateBarang, deleteBarang, addBarang };

export default barangServices;
