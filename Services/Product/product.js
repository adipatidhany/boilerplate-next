import axios from "axios";

const getInvoice = async () => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const url = `${process.env.urlInvoice}/report`
  await axios
    .get(url, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
    });
  return data;
};

const updateProduct = async (invoiceId, name) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const url = `${process.env.urlProduct}/${invoiceId}`;
  const requestBody = {
    name
  };
  await axios
    .patch(url, requestBody, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const deleteProduct = async (invoiceId) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const url = `${process.env.urlProduct}/${invoiceId}`;
  await axios
    .delete(url, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const addProduct = async (invoiceId, name) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const requestBody = {
    invoiceId,
    name
  };
  await axios
    .post(process.env.urlProduct, requestBody, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const invoiceServices = { getInvoice, updateProduct, deleteProduct, addProduct };

export default invoiceServices;
