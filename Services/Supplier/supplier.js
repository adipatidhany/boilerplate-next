import axios from "axios";

const getSupplier = async () => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  await axios
    .get(process.env.urlSupplier, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
    });
    console.log(data)
  return data;
};

const updateSupplier = async (supplierId, name, vendor) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const url = `${process.env.urlSupplier}/${supplierId}`;
  const requestBody = {
    name,
    supplierId
  };
  await axios
    .patch(url, requestBody, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const deleteSupplier = async (idSupplier) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const url = `${process.env.urlSupplier}/${idSupplier}`;
  await axios
    .delete(url, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const addSupplier = async (supplierId, name, vendor) => {
  let data = {};
  const config = {
    headers: {
      token: process.env.key,
    },
  };
  const requestBody = {
    supplierId,
    vendor,
    name,
  };
  await axios
    .post(process.env.urlSupplier, requestBody, config)
    .then((result) => {
      data = result.data;
    })
    .catch((err) => {
      data = err.response.data;
      console.log(data);
    });
  return data;
};

const barangServices = { getSupplier, updateSupplier, deleteSupplier, addSupplier };

export default barangServices;
