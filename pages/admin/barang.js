import { forwardRef, useState, useEffect } from "react";
import Link from "next/link";
import MaterialTable from "material-table";
import Layout from "../../components/Layout/layout";
import { columns } from "../../components/Table/columnsMasterBarang";
import barangServices from "../../Services/Barang/barang";
import { makeStyles } from "@material-ui/core/styles";

import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "20ch",
    },
  },
  btnAdd: {
    marginTop: 20,
    marginBottom: 20,
  },
}));

const MasterBarang = () => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [openMessage, setOpenMessage] = useState(false);
  const [openUpdateBarang, setOpenUpdateBarang] = useState(false);
  const [supplierId, setSupplierId] = useState("");
  const [idBarang, setIdBarang] = useState("");
  const [name, setName] = useState("");
  const [quantity, setQuantity] = useState(0);
  const [dimension, setDimension] = useState("");
  const [message, setMessage] = useState("");
  const [status, setStatus] = useState("");
  const [dataUpdate, setDataUpdate] = useState({});

  const getDataBarang = async () => {
    setLoading(true);
    const resBarang = await barangServices.getBarang();
    console.log(resBarang.data);
    setData(resBarang.data);
    setLoading(false);
  };

  const addBarang = async () => {
    setLoading(true);
    const resAddBarang = await barangServices.addBarang(
      supplierId,
      idBarang,
      name,
      quantity,
      dimension
    );
    if (resAddBarang.code === 200) {
      const newData = [resAddBarang.data[0], ...data];
      setData(newData);
    }
    setMessage(resAddBarang.message);
    setStatus(resAddBarang.status);
    setSupplierId("");
    setIdBarang("");
    setName("");
    setQuantity(0);
    setDimension("");
    setLoading(false);
    setOpenMessage(true);
  };

  const deleteBarang = async (rowData) => {
    setLoading(true);
    const resAddBarang = await barangServices.deleteBarang(rowData.idBarang);
    if (resAddBarang.code === 200) {
      const arrayTemp = [rowData];
      const newData = data.filter((x) => !arrayTemp.includes(x));
      setData(newData);
    }
    setMessage(resAddBarang.message);
    setStatus(resAddBarang.status);
    setLoading(false);
    setOpenMessage(true);
  };

  const updateBarang = async (rowData) => {
    setLoading(true);
    name === '' ? name = rowData.name : name = name;
    quantity === 0 ? quantity = rowData.quantity : quantity = quantity;
    const resAddBarang = await barangServices.updateBarang(
      rowData.idBarang,
      name,
      quantity
    );
    if (resAddBarang.code === 200) {
      const index = data.indexOf(rowData);
      let tempData = [...data];
      tempData[index] = resAddBarang.data[0];
      console.log(tempData)
      setData(tempData);
      console.log(data);
    }
    setMessage(resAddBarang.message);
    setStatus(resAddBarang.status);
    setLoading(false);
    setOpenMessage(true);
    setOpenUpdateBarang(false);
  };

  useEffect(() => {
    getDataBarang();
  }, []);

  return (
    <Layout>
      <div>
        <h2>Master Barang</h2>
        <form className={classes.root}>
          <div>
            <TextField
              required
              id="id-supplier"
              label="ID Supplier"
              onChange={(e) => setSupplierId(e.target.value)}
            />
            <TextField
              required
              id="id-barang"
              label="ID Barang"
              onChange={(e) => setIdBarang(e.target.value)}
            />
            <TextField
              required
              id="name"
              label="Nama Barang"
              onChange={(e) => setName(e.target.value)}
            />
            <TextField
              required
              id="quantity"
              label="Stok"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setQuantity(e.target.value)}
            />
            <TextField
              required
              id="dimension"
              label="Dimension"
              onChange={(e) => setDimension(e.target.value)}
            />
          </div>
          <div>
            <Button
              onClick={addBarang}
              variant="contained"
              color="primary"
              className={classes.btnAdd}
            >
              Simpan
            </Button>
          </div>
        </form>
        <MaterialTable
          title="Data Master Barang"
          columns={columns}
          data={data}
          options={{
            actionsColumnIndex: -1,
            exportButton: true,
            search: true,
            sorting: true,
            grouping: true,
          }}
          actions={[
            {
              icon: "delete",
              tooltip: "Delete Barang",
              onClick: (event, rowData) => {
                deleteBarang(rowData);
              },
            },
            {
              icon: "edit",
              tooltip: "Edit Barang",
              onClick: (event, rowData) => {
                setDataUpdate(rowData);
                setOpenUpdateBarang(true);
              },
            },
          ]}
        />
        <div>
          <Dialog
            open={openMessage}
            onClose={() => setOpenMessage(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{status}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                {message}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => setOpenMessage(false)}
                color="primary"
                autoFocus
              >
                Ok
              </Button>
            </DialogActions>
          </Dialog>
        </div>
        <div>
          <Dialog
            open={openUpdateBarang}
            onClose={() => setOpenUpdateBarang(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Update Barang</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <form className={classes.root}>
                  <div>
                    <TextField
                      required
                      id="name"
                      label="Nama Barang"
                      onChange={(e) => setName(e.target.value)}
                      defaultValue={dataUpdate.name}
                    />

                    <TextField
                      required
                      id="quantity"
                      label="Stok"
                      type="number"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      defaultValue={dataUpdate.quantity}
                      onChange={(e) => setQuantity(e.target.value)}
                    />
                  </div>
                </form>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => setOpenUpdateBarang(false)}
                color="primary"
              >
                Batal
              </Button>
              <Button
                onClick={() => updateBarang(dataUpdate)}
                color="primary"
                autoFocus
              >
                Simpan
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      </div>
    </Layout>
  );
};

export default MasterBarang;
