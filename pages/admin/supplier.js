import { forwardRef, useState, useEffect } from "react";
import Link from "next/link";
import MaterialTable from "material-table";
import Layout from "../../components/Layout/layout";
import { columns } from "../../components/Table/columnsMasterSupplier";
import supplierServices from "../../Services/Supplier/supplier";
import { makeStyles } from "@material-ui/core/styles";

import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "20ch",
    },
  },
  btnAdd: {
    marginTop: 20,
    marginBottom: 20,
  },
}));

const MasterSupplier = () => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [openMessage, setOpenMessage] = useState(false);
  const [openUpdateSupplier, setOpenUpdateSupplier] = useState(false);
  const [supplierId, setSupplierId] = useState("");
  let [name, setName] = useState("");
  let [vendor, setVendor] = useState("");
  const [message, setMessage] = useState("");
  const [status, setStatus] = useState("");
  const [dataUpdate, setDataUpdate] = useState({});

  const getDataSupplier = async () => {
    setLoading(true);
    const resSupplier = await supplierServices.getSupplier();
    console.log(resSupplier.data);
    setData(resSupplier.data);
    setLoading(false);
  };

  const addSupplier = async () => {
    setLoading(true);
    const resAddSupplier = await supplierServices.addSupplier(
      supplierId,
      name,
      vendor,
    );
    if (resAddSupplier.code === 200) {
      const newData = [resAddSupplier.data[0], ...data];
      setData(newData);
    }
    setMessage(resAddSupplier.message);
    setStatus(resAddSupplier.status);
    setSupplierId("");
    setName("");
    setVendor("");
    setLoading(false);
    setOpenMessage(true);
  };

  const deleteSupplier = async (rowData) => {
    setLoading(true);
    const resAddSupplier = await supplierServices.deleteSupplier(rowData.supplierId);
    if (resAddSupplier.code === 200) {
      const arrayTemp = [rowData];
      const newData = data.filter((x) => !arrayTemp.includes(x));
      setData(newData);
    }
    setMessage(resAddSupplier.message);
    setStatus(resAddSupplier.status);
    setLoading(false);
    setOpenMessage(true);
  };

  const updateSupplier = async (rowData) => {
    setLoading(true);
    name === '' ? name = rowData.name : name = name;
    vendor === '' ? vendor = rowData.vendor : vendor = vendor;
    console.log(name, vendor)
    const resAddSupplier = await supplierServices.updateSupplier(
      rowData.supplierId,
      name,
      vendor,
    );
    if (resAddSupplier.code === 200) {
      const index = data.indexOf(rowData);
      let tempData = [...data];
      tempData[index] = resAddSupplier.data[0];
      console.log(tempData)
      setData(tempData);
      console.log(data);
    }
    setMessage(resAddSupplier.message);
    setStatus(resAddSupplier.status);
    setName("");
    setVendor("");
    setLoading(false);
    setOpenMessage(true);
    setOpenUpdateSupplier(false);
  };

  useEffect(() => {
    getDataSupplier();
  }, []);

  return (
    <Layout>
      <div>
        <h2>Master Supplier</h2>
        <form className={classes.root}>
          <div>
            <TextField
              required
              id="id-supplier"
              label="ID Supplier"
              onChange={(e) => setSupplierId(e.target.value)}
            />
            <TextField
              required
              id="name"
              label="Nama Supplier"
              onChange={(e) => setName(e.target.value)}
            />
            <TextField
              required
              id="vendor"
              label="Vendor"
              onChange={(e) => setVendor(e.target.value)}
            />
          </div>
          <div>
            <Button
              onClick={addSupplier}
              variant="contained"
              color="primary"
              className={classes.btnAdd}
            >
              Simpan
            </Button>
          </div>
        </form>
        <MaterialTable
          title="Data Master Supplier"
          columns={columns}
          data={data}
          options={{
            actionsColumnIndex: -1,
            exportButton: true,
            search: true,
            sorting: true,
            grouping: true,
          }}
          actions={[
            {
              icon: "delete",
              tooltip: "Delete Supplier",
              onClick: (event, rowData) => {
                deleteSupplier(rowData);
              },
            },
            {
              icon: "edit",
              tooltip: "Edit Supplier",
              onClick: (event, rowData) => {
                setDataUpdate(rowData);
                setOpenUpdateSupplier(true);
              },
            },
          ]}
        />
        <div>
          <Dialog
            open={openMessage}
            onClose={() => setOpenMessage(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{status}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                {message}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => setOpenMessage(false)}
                color="primary"
                autoFocus
              >
                Ok
              </Button>
            </DialogActions>
          </Dialog>
        </div>
        <div>
          <Dialog
            open={openUpdateSupplier}
            onClose={() => setOpenUpdateSupplier(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Update Supplier</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <form className={classes.root}>
                  <div>
                    <TextField
                      required
                      id="name"
                      label="Nama Supplier"
                      onChange={(e) => setName(e.target.value)}
                      defaultValue={dataUpdate.name}
                    />

                    <TextField
                      required
                      id="vendor"
                      label="Vendor"
                      defaultValue={dataUpdate.vendor}
                      onChange={(e) => setVendor(e.target.value)}
                    />
                  </div>
                </form>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => setOpenUpdateSupplier(false)}
                color="primary"
              >
                Batal
              </Button>
              <Button
                onClick={() => updateSupplier(dataUpdate)}
                color="primary"
                autoFocus
              >
                Simpan
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      </div>
    </Layout>
  );
};

export default MasterSupplier;
