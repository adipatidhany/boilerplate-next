import { forwardRef, useState, useEffect } from "react";
import Link from "next/link";
import MaterialTable from "material-table";
import Layout from "../../components/Layout/layout";
import { columns } from "../../components/Table/columnsMasterInvoice";
import invoiceServices from "../../Services/Invoice/invoice";
import { makeStyles } from "@material-ui/core/styles";

import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  InputLabel,
  MenuItem,
  FormHelperText,
  FormControl,
  Select,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "20ch",
    },
  },
  btnAdd: {
    marginTop: 20,
    marginBottom: 20,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

const Invoices = () => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [openMessage, setOpenMessage] = useState(false);
  const [openUpdateInvoice, setOpenUpdateInvoice] = useState(false);
  const [invoiceId, setInvoiceId] = useState("");
  let [name, setName] = useState("");
  let [keterangan, setKeterangan] = useState("Pending")
  const [message, setMessage] = useState("");
  const [status, setStatus] = useState("");
  const [dataUpdate, setDataUpdate] = useState({});

  const getDataInvoice = async () => {
    setLoading(true);
    const resInvoice = await invoiceServices.getInvoice();
    setData(resInvoice.data);
    setLoading(false);
  };

  const addInvoice = async () => {
    setLoading(true);
    const resAddInvoice = await invoiceServices.addInvoice(
      invoiceId,
      name,
    );
    if (resAddInvoice.code === 200) {
      const newData = [resAddInvoice.data[0], ...data];
      setData(newData);
    }
    setMessage(resAddInvoice.message);
    setStatus(resAddInvoice.status);
    setInvoiceId("");
    setName("");
    setLoading(false);
    setOpenMessage(true);
  };

  const deleteInvoice = async (rowData) => {
    setLoading(true);
    const resAddInvoice = await invoiceServices.deleteInvoice(rowData.invoiceId);
    if (resAddInvoice.code === 200) {
      const arrayTemp = [rowData];
      const newData = data.filter((x) => !arrayTemp.includes(x));
      setData(newData);
    }
    setMessage(resAddInvoice.message);
    setStatus(resAddInvoice.status);
    setLoading(false);
    setOpenMessage(true);
  };

  const updateInvoice = async (rowData) => {
    setLoading(true);
    const resAddInvoice = await invoiceServices.updateInvoice(
      rowData.invoiceId,
      keterangan
    );
    if (resAddInvoice.code === 200) {
      const index = data.indexOf(rowData);
      let tempData = [...data];
      tempData[index] = resAddInvoice.data[0];
      console.log(tempData)
      setData(tempData);
      console.log(data);
    }
    setMessage(resAddInvoice.message);
    setStatus(resAddInvoice.status);
    setKeterangan("Pending");
    setLoading(false);
    setOpenMessage(true);
    setOpenUpdateInvoice(false);
  };

  useEffect(() => {
    getDataInvoice();
  }, []);

  return (
    <Layout>
      <div>
        <h2>Master Invoice</h2>
        <form className={classes.root}>
          <div>
            <TextField
              required
              id="id-invoice"
              label="ID Invoice"
              onChange={(e) => setInvoiceId(e.target.value)}
            />
            <TextField
              required
              id="name"
              label="Nama Invoice"
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div>
            <Button
              onClick={addInvoice}
              variant="contained"
              color="primary"
              className={classes.btnAdd}
            >
              Simpan
            </Button>
          </div>
        </form>
        <MaterialTable
          title="Data Master Invoice"
          columns={columns}
          data={data}
          options={{
            actionsColumnIndex: -1,
            exportButton: true,
            search: true,
            sorting: true,
            grouping: true,
          }}
          actions={[
            {
              icon: "delete",
              tooltip: "Delete Invoice",
              onClick: (event, rowData) => {
                deleteInvoice(rowData);
              },
            },
            {
              icon: "edit",
              tooltip: "Edit Invoice",
              onClick: (event, rowData) => {
                setDataUpdate(rowData);
                setOpenUpdateInvoice(true);
              },
            },
          ]}
        />
        <div>
          <Dialog
            open={openMessage}
            onClose={() => setOpenMessage(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{status}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                {message}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => setOpenMessage(false)}
                color="primary"
                autoFocus
              >
                Ok
              </Button>
            </DialogActions>
          </Dialog>
        </div>
        <div>
          <Dialog
            open={openUpdateInvoice}
            onClose={() => setOpenUpdateInvoice(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Update Invoice</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <form className={classes.root}>
                  <FormControl className={classes.formControl}>
                    <InputLabel id="change-keterangan">Keterangan</InputLabel>
                    <Select
                      labelId="change-keterangan"
                      id="keterangan"
                      value={keterangan}
                      onChange={(e) => setKeterangan(e.target.value)}
                    >
                      <MenuItem value={'Pending'}>Pending</MenuItem>
                      <MenuItem value={'Diterima'}>Diterima</MenuItem>
                      <MenuItem value={'Batal'}>Batal</MenuItem>
                    </Select>
                  </FormControl>
                </form>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => setOpenUpdateInvoice(false)}
                color="primary"
              >
                Batal
              </Button>
              <Button
                onClick={() => updateInvoice(dataUpdate)}
                color="primary"
                autoFocus
              >
                Simpan
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      </div>
    </Layout>
  );
};

export default Invoices;
