import { forwardRef, useState, useEffect } from "react";
import Link from "next/link";
import MaterialTable from "material-table";
import Layout from "../../components/Layout/layout";
import { columns } from "../../components/Table/columnsMasterInvoice";
import { columnsProduct } from "../../components/Table/columnsProduct";
import productServices from "../../Services/Product/product";
import { makeStyles } from "@material-ui/core/styles";

import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "20ch",
    },
  },
  btnAdd: {
    marginTop: 20,
    marginBottom: 20,
  },
}));

const Product = () => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  let [product, setProduct] = useState([]);
  const [loading, setLoading] = useState(false);
  const [openMessage, setOpenMessage] = useState(false);
  const [openUpdateInvoice, setOpenUpdateInvoice] = useState(false);
  const [invoiceId, setInvoiceId] = useState("");
  const [barangId, setBarangId] = useState("");
  const [quantity, setQuantity] = useState(0);
  let [name, setName] = useState("");
  const [message, setMessage] = useState("");
  const [status, setStatus] = useState("");
  const [dataUpdate, setDataUpdate] = useState({});

  const getDataInvoice = async () => {
    setLoading(true);
    const resInvoice = await productServices.getInvoice();
    setData(resInvoice.data);
    setLoading(false);
  };

  const addProduct = async () => {
    setLoading(true);
    const resAddProduct = await productServices.addProduct(
      invoiceId,
      barangId,
      quantity,
    );
    if (resAddProduct.code === 200) {
      const newData = [resAddProduct.data[0], ...data];
      setData(newData);
    }
    setMessage(resAddProduct.message);
    setStatus(resAddProduct.status);
    setProductId("");
    setName("");
    setLoading(false);
    setOpenMessage(true);
  };

  const deleteProduct = async (rowData) => {
    setLoading(true);
    const resAddProduct = await productServices.deleteProduct(rowData.invoiceId);
    if (resAddProduct.code === 200) {
      const arrayTemp = [rowData];
      const newData = data.filter((x) => !arrayTemp.includes(x));
      setData(newData);
    }
    setMessage(resAddProduct.message);
    setStatus(resAddProduct.status);
    setLoading(false);
    setOpenMessage(true);
  };

  const updateProduct = async (rowData) => {
    setLoading(true);
    name === '' ? name = rowData.name : name = name;
    vendor === '' ? vendor = rowData.vendor : vendor = vendor;
    console.log(name, vendor)
    const resAddProduct = await productServices.updateProduct(
      rowData.invoiceId,
      name,
      vendor,
    );
    if (resAddProduct.code === 200) {
      const index = data.indexOf(rowData);
      let tempData = [...data];
      tempData[index] = resAddProduct.data[0];
      console.log(tempData)
      setData(tempData);
      console.log(data);
    }
    setMessage(resAddProduct.message);
    setStatus(resAddProduct.status);
    setName("");
    setVendor("");
    setLoading(false);
    setOpenMessage(true);
    setOpenUpdateInvoice(false);
  };

  useEffect(() => {
    getDataInvoice();
  }, []);

  return (
    <Layout>
      <div>
        <h2>Product</h2>
        <form className={classes.root}>
          <div>
            <TextField
              required
              id="id-invoice"
              label="ID Invoice"
              onChange={(e) => setInvoiceId(e.target.value)}
            />
            <TextField
              required
              id="id-barang"
              label="ID Barang"
              onChange={(e) => setBarangId(e.target.value)}
            />
            <TextField
              required
              id="quantity"
              label="Stok"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => setQuantity(e.target.value)}
            />
          </div>
          <div>
            <Button
              onClick={addInvoice}
              variant="contained"
              color="primary"
              className={classes.btnAdd}
            >
              Simpan
            </Button>
          </div>
        </form>
        <MaterialTable
          title="Data Product"
          columns={columns}
          data={data}
          options={{
            actionsColumnIndex: -1,
            exportButton: true,
            search: true,
            sorting: true,
            grouping: true,
          }}
          detailPanel={rowData => {
            console.log(rowData)
            return (
              <MaterialTable
                title="List Daftar Product"
                columns={columnsProduct}
                data={rowData.product}
                options={{
                  actionsColumnIndex: -1,
                  exportButton: true,
                  search: true,
                  sorting: true,
                  grouping: true,
                }}
                actions={[
                  {
                    icon: "delete",
                    tooltip: "Delete Invoice",
                    onClick: (event, rowData) => {
                      deleteInvoice(rowData);
                    },
                  },
                  {
                    icon: "edit",
                    tooltip: "Edit Invoice",
                    onClick: (event, rowData) => {
                      setDataUpdate(rowData);
                      setOpenUpdateInvoice(true);
                    },
                  },
                ]}
              />
            )
          }}
          onRowClick={(event, rowData, togglePanel) => togglePanel()}
        />
        <div>
          <Dialog
            open={openMessage}
            onClose={() => setOpenMessage(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{status}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                {message}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => setOpenMessage(false)}
                color="primary"
                autoFocus
              >
                Ok
              </Button>
            </DialogActions>
          </Dialog>
        </div>
        <div>
          <Dialog
            open={openUpdateInvoice}
            onClose={() => setOpenUpdateInvoice(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Update Invoice</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <form className={classes.root}>
                  <div>
                    <TextField
                      required
                      id="name"
                      label="Nama Invoice"
                      onChange={(e) => setName(e.target.value)}
                      defaultValue={dataUpdate.name}
                    />

                    <TextField
                      required
                      id="vendor"
                      label="Vendor"
                      defaultValue={dataUpdate.vendor}
                      onChange={(e) => setVendor(e.target.value)}
                    />
                  </div>
                </form>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => setOpenUpdateInvoice(false)}
                color="primary"
              >
                Batal
              </Button>
              <Button
                onClick={() => updateInvoice(dataUpdate)}
                color="primary"
                autoFocus
              >
                Simpan
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      </div>
    </Layout>
  );
};

export default Product;
